#include "list_test.h"
#include "lib_lst.h"

#include <time.h>
#include <limits.h>

int	main(int ac, char **av)
{
	int			i;
	int			size;
	int			fd;
	int			seed;
	t_stack		*new;
	t_stack		*lst;
	t_stack		*ptr;

	
	fd = open("/dev/urandom", O_RDONLY);
	read(fd, &seed, 4);
	close(fd);
	if (ac == 1 || ac > 2)
		return (0);
	size = ft_atoi(av[1]);
	if (size <= 0 || size > INT_MAX)
		return (0);
	i = 0;
	lst = NULL;
	srand(seed);
	while (i < size)
	{
		new = new_rand_elem();
		if (ft_lstadd_back_nodouble(&lst, new) == 0)
			i++;
		else
			ft_lstdelone(new, free);
	}
	ptr = lst;
	while (ptr != NULL)
	{
		ft_itoa(*(ptr->data));
		ft_putchar(' ');
		ptr = ptr->next;
	}
	ft_putchar('\n');
	ft_lstclear(&lst, free);
	return (0);
}
