#include "list_test.h"
#define ISNUM "0123456789"

void	ft_putchar(char c)
{
	write(1, &c, 1);
}

int	ft_putterm(int ptr)
{
	write(1, &ptr, 4);
	return (0);
}

void	ft_putstr(char *str)
{
	int	i;

	i = 0;
	while (str[i])
	{
		ft_putchar(str[i]);
		i++;
	}
}

void	ft_itoa(int	nbr)
{
	char		c;
	long int	nb;

	if (nbr < 0)
	{
		ft_putchar('-');
		nb = -nbr;
	}
	else
		nb = nbr;
	if (nb < 10)
	{
		c = nb + 48;
		ft_putchar(c);
	}
	else
	{
		ft_itoa(nb / 10);
		ft_itoa(nb % 10);
	}
}

int		ft_isset(char c, char *set)
{
	int	i;

	i = 0;
	while (set[i])
	{
		if (c == set[i])
			return (i);
		i++;
	}
	return (-1);
}

int		ft_isnum(char *str)
{
	int	i;

	i = 0;
	if (str[i] == '-')
		i++;
	while (ft_isset(str[i], ISNUM) >= 0)
		i++;
	if (str[i])
		return (-1);
	return (0);
}

char	*ft_strdup(const char *s1)
{
	size_t	i;
	char	*cpy;

	i = 0;
	while (s1[i])
		i++;
	if (!(cpy = malloc((i + 1) * sizeof(char))))
		return (NULL);
	i = 0;
	while (s1[i])
	{
		cpy[i] = s1[i];
		i++;
	}
	cpy[i] = '\0';
	return (cpy);
}

long int		ft_atoi(char *str)
{
	long int	nbr;
	long int	tmp;
	long int	sign;
	int			i;

	nbr = 0;
	i = 0;
	sign = 1;
	if (str[i] == '-')
	{
		sign *= -1;
		i++;
	}
	while ((tmp = ft_isset(str[i], ISNUM)) >= 0)
	{
		nbr *= 10;
		nbr += tmp;
		i++;
	}
	return (nbr * sign);
}
