# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: abenoit <marvin@42.fr>                     +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2020/09/23 18:47:34 by abenoit           #+#    #+#              #
#    Updated: 2021/04/16 11:39:20 by abenoit          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

OS = $(shell uname -s)

ifeq ($(OS),Darwin)
	OS_DEF = -D os_mac
else
	OS_DEF = -D os_linux
endif

CC = clang

CL_SRC += create_list.c
CL_SRC += utils.c

CL_OBJ=$(addprefix $(OBJ_DIR), $(CL_SRC:.c=.o))

CFLAGS = -Wall -Werror -Wextra

LIB_LST_DIR = liblst/

INC = -I inc -I $(LIB_LST_DIR)inc

INC_LIB += -lc
INC_LIB += -L$(LIB_LST_DIR) -llst

CL_BIN_NAME = create_list

RM = rm -f

ifeq ($(d), 0)
	CFLAGS	+= -fsanitize=address
	LIB_LST_FLAGS = d=0
endif

all: $(CL_BIN_NAME)

$(CL_BIN_NAME): LIB_LST $(CL_OBJ) $(OBJ)
	$(CC) $(CFLAGS) $(CL_OBJ) $(OBJ) -o $(CL_BIN_NAME) $(INC) $(INC_LIB)

%.o:	%.c
	$(CC) $(INC) $(CFLAGS) -c $< -o $@

LIB_LST:
		make -C $(LIB_LST_DIR) $(LIB_LST_FLAGS)

clean:
	$(RM) $(CL_OBJ)

fclean: clean
	$(RM) $(CL_BIN_NAME)
	make -C $(LIB_LST_DIR) fclean

re: fclean all

.PHONY: clean fclean re
