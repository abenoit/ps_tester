#ifndef PUSH_SWAP_H
# define PUSH_SWAP_H

# include <unistd.h>
# include <stdlib.h>

# define ARG_ERROR		-1
# define MALLOC_ERROR	-2
# define INPUT_ERROR	-3

# define SORTING_OK		-1
# define B_NOT_EMPTY	-2

# define OP_1	"spr"
# define SWP_2	"abs"
# define PSH_2	"ab"
# define ROT_2	"abr"

# ifndef T_LIST_FORMAT
#  define T_LIST_FORMAT

typedef struct	s_stack
{
	int				*data;
	struct s_stack	*next;
}				t_stack;

typedef int (*t_func)(char *line, t_stack **lst_a, t_stack **lst_b);

# endif

int		func_selecta(char *line, t_stack **lst_a, t_stack **lst_b);

int		rec_gnl(int fd, char **line);

void	swap(t_stack **lst_a);
void	push(t_stack **lst_a, t_stack **lst_b);
void	rotate(t_stack **lst);
void	r_rotate(t_stack **lst);
int 	check_order(t_stack *lst);
int 	check_order_reverse(t_stack *lst);
int 	check_end(t_stack *lst_a, t_stack *lst_b);

int	swap_a(char *line, t_stack **lst_a, t_stack **lst_b);
int	swap_b(char *line, t_stack **lst_a, t_stack **lst_b);
int	swap_s(char *line, t_stack **lst_a, t_stack **lst_b);

int	push_a(char *line, t_stack **lst_a, t_stack **lst_b);
int	push_b(char *line, t_stack **lst_a, t_stack **lst_b);

int	rotate_a(char *line, t_stack **lst_a, t_stack **lst_b);
int	rotate_b(char *line, t_stack **lst_a, t_stack **lst_b);
int	rotate_r(char *line, t_stack **lst_a, t_stack **lst_b);

int	r_rotate_a(char *line, t_stack **lst_a, t_stack **lst_b);
int	r_rotate_b(char *line, t_stack **lst_a, t_stack **lst_b);
int	r_rotate_r(char *line, t_stack **lst_a, t_stack **lst_b);

# endif
