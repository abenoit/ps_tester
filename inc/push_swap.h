#ifndef PUSH_SWAP_H
# define PUSH_SWAP_H

# include <unistd.h>
# include <stdlib.h>

# define ARG_ERROR		-1
# define MALLOC_ERROR	-2
# define INPUT_ERROR	-3

# define SORTING_OK		-1
# define B_NOT_EMPTY	-2
# define MIN_NOT_FIRST	-3
# define MAX_NOT_FIRST	-3

# define OP_1	"spr"
# define SWP_2	"abs"
# define PSH_2	"ab"
# define ROT_2	"abr"

# ifndef T_LIST_FORMAT
#  define T_LIST_FORMAT

typedef struct	s_stack
{
	int				*data;
	struct s_stack	*next;
}				t_stack;
# endif


typedef int (*t_func)(char *line, t_stack **lst_a, t_stack **lst_b);

int		func_launcher(t_stack **lst_a, t_stack **lst_b);

void	move_top_a(t_stack **lst, int val);
void	move_top_b(t_stack **lst, int val);
void	get_next_median_low(t_stack **lst_a, t_stack **lst_b, int n, int median);
void	get_next_median_high(t_stack **lst_a, t_stack **lst_b, int n, int median);
void	split_list_low(t_stack **lst_a, t_stack **lst_b, int n, int median);
void	split_list_high(t_stack **lst_a, t_stack **lst_b, int n, int median);
void	split_list_b_low(t_stack **lst_a, t_stack **lst_b, int n, int median);
void	split_list_b_low_2(t_stack **lst_a, t_stack **lst_b, int n, int median);
void	split_list_b_high(t_stack **lst_a, t_stack **lst_b, int n, int median);
void	sort_two_a(t_stack **lst_a, t_stack **lst_b);
void	sort_two_b(t_stack **lst_a, t_stack **lst_b);
void	sort_three_a(t_stack **lst_a, t_stack **lst_b);
void	sort_three_b(t_stack **lst_a, t_stack **lst_b);
void	sort_four_a(t_stack **lst_a, t_stack **lst_b);
void	sort_four_b(t_stack **lst_a, t_stack **lst_b);
void	sort_five_a(t_stack **lst_a, t_stack **lst_b);
void	sort_five_b(t_stack **lst_a, t_stack **lst_b);
void	sort_three_a_dual(t_stack **lst_a, t_stack **lst_b);
void	sort_four_a_dual(t_stack **lst_a, t_stack **lst_b);
void	sort_five_a_dual(t_stack **lst_a, t_stack **lst_b);

void	swap(t_stack **lst_a);
void	push(t_stack **lst_a, t_stack **lst_b);
void	rotate(t_stack **lst);
void	r_rotate(t_stack **lst);
int 	check_order(t_stack *lst);
int 	check_order_reverse(t_stack *lst);
int 	check_end(t_stack *lst_a, t_stack *lst_b);


int	swap_a(char *line, t_stack **lst_a, t_stack **lst_b);
int	swap_b(char *line, t_stack **lst_a, t_stack **lst_b);
int	swap_s(char *line, t_stack **lst_a, t_stack **lst_b);

int	push_a(char *line, t_stack **lst_a, t_stack **lst_b);
int	push_b(char *line, t_stack **lst_a, t_stack **lst_b);

int	rotate_a(char *line, t_stack **lst_a, t_stack **lst_b);
int	rotate_b(char *line, t_stack **lst_a, t_stack **lst_b);
int	rotate_r(char *line, t_stack **lst_a, t_stack **lst_b);

int	r_rotate_a(char *line, t_stack **lst_a, t_stack **lst_b);
int	r_rotate_b(char *line, t_stack **lst_a, t_stack **lst_b);
int	r_rotate_r(char *line, t_stack **lst_a, t_stack **lst_b);

# endif
