#ifndef LIST_TEST_H
# define LIST_TEST_H

#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>

# ifndef T_LIST_FORMAT
#  define T_LIST_FORMAT

typedef struct	s_stack
{
	int				*data;
	struct s_stack	*next;
}				t_stack;

# endif

/*
**	test functions
*/

int		stack_test(int size);
int		call_stack_test(int *state);

/*
**	stack utils
*/

int		ft_int_comp(void *n1, void *n2);
void	ft_add(void *content);
void	*ft_yolo(void *content);
int		print_lst_err(t_stack *first, int err_code);
int		print_lst_nempty(t_stack *first, int k);
int		visual_out(char *line, t_stack *lst_a, t_stack *lst_b);
int		output(t_stack *lst_a, t_stack *lst_b);
int		err_output(t_stack *lst_a, t_stack *lst_b, int err_code);

/*
**	util functions
*/

void	ft_putchar(char c);
int		ft_putterm(int ptr);
void	ft_putstr(char *str);
void	ft_itoa(int	nbr);
int		ft_isset(char c, char *set);
int		ft_isnum(char *str);
long int		ft_atoi(char *str);
char	*ft_strdup(const char *s1);


/*
**	test functions
*/

int		rec_gnl(int fd, char **line);
int		full_test(void);

#endif
