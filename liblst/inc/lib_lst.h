/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lib_lst.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abenoit <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/07 16:18:24 by abenoit           #+#    #+#             */
/*   Updated: 2021/04/16 23:17:25 by abenoit          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIB_LST_H
# define LIB_LST_H

#include <unistd.h>

# ifndef T_LIST_FORMAT
#  define T_LIST_FORMAT

typedef struct	s_stack
{
	int				*data;
	struct s_stack	*next;
}				t_stack;

# endif

t_stack				*ft_lstnew(int *data);
void				ft_lstadd_front(t_stack **alst, t_stack *new);
int					ft_lstsize(t_stack *lst);
t_stack				*ft_lstlast(t_stack *lst);
void				ft_lstadd_back(t_stack **alst, t_stack *new);
void				ft_lstdelone(t_stack *lst, void(*del)(void*));
void				ft_lstclear(t_stack **lst, void (*del)(void*));
void				ft_lstiter(t_stack *lst, void (*f)(void *));
t_stack				*ft_lstmap(t_stack *lst, void *(*f)(void *),
						void (*del)(void *));

int					ft_lstmax(t_stack *lst);
int					ft_lstmin(t_stack *lst);
int					ft_lstget_val(t_stack *lst, int val);
int					ft_lstget_index(t_stack *lst, int val);
int					ft_lstadd_back_nodouble(t_stack **alst, t_stack *new);

int					print_lst(t_stack *first);
int					print_lst_2(t_stack *first, int k);
int					*new_fixed_num(int val);
t_stack				*new_fixed_elem(int val);
int					*new_rand_num(void);
t_stack				*new_rand_elem(void);
t_stack				*ft_lstcpy(t_stack *lst);
void				stack_remove_val(t_stack **lst, int val);
int					stack_next_val(t_stack **lst, int val);
int					stack_next_val_reverse(t_stack **lst, int val);
int					range_from_top(t_stack **lst, int first, int last);
int					range_from_bottom(t_stack *ptr, int first, int last, int i);
int					median_last_val(t_stack *ptr, int median, int i);
int					median_next_val(t_stack **lst, int median);
int					median_last_val_reverse(t_stack *ptr, int median, int i);
int					median_next_val_reverse(t_stack **lst, int median);

#endif
