#include "lib_lst.h"

int	ft_lstget_val(t_stack *lst, int val)
{
	int		i;
	t_stack	*ptr;

	i = 0;
	ptr = lst;
	while (ptr != NULL)
	{
		if (*(ptr->data) == val)
			return (i);
		i++;
		ptr = ptr->next;
	}
	return (-1);
}
