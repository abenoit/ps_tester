
#include "lib_lst.h"

int	ft_lstget_index(t_stack *lst, int index)
{
	int		i;
	t_stack	*ptr;

	i = 0;
	ptr = lst;
	while (ptr != NULL)
	{
		if (i == index)
			return (*(ptr->data));
		i++;
		ptr = ptr->next;
	}
	return (-1);
}
