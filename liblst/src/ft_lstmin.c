#include "lib_lst.h"

int	ft_lstmin(t_stack *lst)
{
	t_stack	*ptr;
	int		cur_min;

	if (lst == NULL)
		return (0);
	ptr = lst;
	cur_min = *(ptr->data);
	while (ptr != NULL)
	{
		if (cur_min > *(ptr->data))
			cur_min = *(ptr->data);
		ptr = ptr->next;
	}
	return (cur_min);
}
