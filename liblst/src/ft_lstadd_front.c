/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstadd_front.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abenoit <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/06 17:41:29 by abenoit           #+#    #+#             */
/*   Updated: 2021/03/05 12:07:29 by abenoit          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lib_lst.h"

void	ft_lstadd_front(t_stack **alst, t_stack *new)
{
	if (new == NULL || alst == NULL)
		return ;
	new->next = *alst;
	*alst = new;
}
