/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstadd_back.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abenoit <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/07 12:31:51 by abenoit           #+#    #+#             */
/*   Updated: 2021/03/05 12:07:25 by abenoit          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lib_lst.h"

int	ft_lstadd_back_nodouble(t_stack **alst, t_stack *new)
{
	t_stack *first;

	if (new == NULL || alst == NULL)
		return (-1);
	if (*alst == NULL)
		*alst = new;
	else
	{
		first = *alst;
		while (first->next != NULL)
		{
			if (*(first->data) == *(new->data))
				return (-1);
			first = first->next;
		}
		if (*(first->data) == *(new->data))
			return (-1);
		first->next = new;
		new->next = NULL;
	}
	return (0);
}
