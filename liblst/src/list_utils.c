
#include <lib_lst.h>
#include <stdlib.h>
#include <stdio.h>
#include <limits.h>

int		*new_fixed_num(int val)
{
	int		*new;

	new = malloc(sizeof(int));
	if (new == NULL)
		return (NULL);
	*new = val;
	return (new);
}

t_stack	*new_fixed_elem(int val)
{
	t_stack	*new;
	int		*content;

	content = new_fixed_num(val);
	new = ft_lstnew(content);
	if (new == NULL)
		return (NULL);
	return (new);
}

int		rand_num(void)
{
	int	 new;

//	new = (rand() % INT_MIN);
	new = (rand() % 10000);
	return (new);
}

int		*new_rand_num(void)
{
	int		*new;

	new = malloc(sizeof(int));
	if (new == NULL)
		return (NULL);
//	*new = (rand() % INT_MIN);
	*new = (rand() % 10000);
	return (new);
}

t_stack	*new_rand_elem(void)
{
	t_stack	*new;
	int		*content;

	content = new_rand_num();
	new = ft_lstnew(content);
	if (new == NULL)
		return (NULL);
	return (new);
}

t_stack	*ft_lstcpy(t_stack *lst)
{
	t_stack	*cpy;

	cpy = NULL;
	while (lst != NULL)
	{
		ft_lstadd_back(&cpy, new_fixed_elem(*(lst->data)));
		lst = lst->next;
	}
	return (cpy);
}

void	stack_remove_val(t_stack **lst, int val)
{
	t_stack *ptr;
	t_stack	*tmp;

	if (lst == NULL || *lst == NULL)
		return ;
	ptr = *lst;
	if (*(ptr->data) == val)
	{
		*lst = ptr->next;
		ft_lstdelone(ptr, free);
		return ;
	}
	tmp = ptr->next;
	while (tmp != NULL)
	{
		if (*(tmp->data) == val)
		{
			ptr->next = tmp->next;
			ft_lstdelone(tmp, free);
			return ;
		}
		ptr = tmp;
		tmp = ptr->next;
	}
}

int		stack_next_val(t_stack **lst, int val)
{
	t_stack	*ptr;
	t_stack	*tmp;
	int		*cur_next;

	ptr = *lst;
	cur_next = NULL;
	if (*(ft_lstlast(*lst)->data) < val && *(ptr->data) > val)
		cur_next = ptr->data;
	tmp = ptr->next;
	while (tmp != NULL)
	{
		if (*(ptr->data) < val && *(tmp->data) > val)
		{
			if (cur_next == NULL)
				cur_next = tmp->data;
			else
			{
				if (*cur_next > *(tmp->data))
					cur_next = tmp->data;
				else
					return (*cur_next);
			}
		}
		ptr = tmp;
		tmp = ptr->next;
	}
	if (cur_next == NULL)
		return (ft_lstmin(*lst));
	else
		return (*cur_next);
}

int		stack_next_val_reverse(t_stack **lst, int val)
{
	t_stack	*ptr;
	t_stack	*tmp;
	int		*cur_next;

	ptr = *lst;
	cur_next = NULL;
	if (*(ft_lstlast(*lst)->data) > val && *(ptr->data) < val)
		cur_next = ptr->data;
	tmp = ptr->next;
	while (tmp != NULL)
	{
		if (*(ptr->data) > val && *(tmp->data) < val)
		{
			if (cur_next == NULL)
				cur_next = tmp->data;
			else
			{
				if (*cur_next < *(tmp->data))
					cur_next = tmp->data;
				else
					return (*cur_next);
			}
		}
		ptr = tmp;
		tmp = ptr->next;
	}
	if (cur_next == NULL)
		return (ft_lstmax(*lst));
	else
		return (*cur_next);
}

int		range_from_top(t_stack **lst, int first, int last)
{
	int		i;
	t_stack	*ptr;

	i = 0;
	ptr = *lst;
	while (ptr != NULL)
	{
		if (*(ptr->data) <= last && *(ptr->data) > first)
			return (i);
		i++;
		ptr = ptr->next;
	}
	return (-1);
}

int		range_from_bottom(t_stack *ptr, int first, int last, int i)
{
	int	ret;

	if (ptr != NULL)
	{
		ret = range_from_bottom(ptr->next, first, last, i + 1);
		if (ret != -1)
			return (ret);
	}
	else
		return (-1);
	if (*(ptr->data) <= last && *(ptr->data) > first)
		return (i);
	else
		return (ret);
}

int		median_last_val(t_stack *ptr, int median, int i)
{
	int	ret;

	if (ptr != NULL)
	{
		ret = median_last_val(ptr->next, median, i + 1);
		if (ret != -1)
			return (ret);
	}
	else
		return (-1);
	if (*(ptr->data) < median)
		return (i);
	else
		return (ret);
}


int		median_next_val(t_stack **lst, int median)
{
	int		i;
	t_stack	*ptr;

	i = 0;
	ptr = *lst;
	while (ptr != NULL)
	{
		if (*(ptr->data) < median)
			return (i);
		i++;
		ptr = ptr->next;
	}
	return (-1);
}

int		median_last_val_reverse(t_stack *ptr, int median, int i)
{
	int	ret;

	if (ptr != NULL)
	{
		ret = median_last_val(ptr->next, median, i + 1);
		if (ret != -1)
			return (ret);
	}
	else
		return (-1);
	if (*(ptr->data) > median)
		return (i);
	else
		return (ret);
}


int		median_next_val_reverse(t_stack **lst, int median)
{
	int		i;
	t_stack	*ptr;

	i = 0;
	ptr = *lst;
	while (ptr != NULL)
	{
		if (*(ptr->data) > median)
			return (i);
		i++;
		ptr = ptr->next;
	}
	return (-1);
}
