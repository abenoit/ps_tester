#include "lib_lst.h"

int	ft_lstmax(t_stack *lst)
{
	t_stack	*ptr;
	int		cur_max;

	if (lst == NULL)
		return (0);
	ptr = lst;
	cur_max = *(ptr->data);
	while (ptr != NULL)
	{
		if (cur_max < *(ptr->data))
			cur_max = *(ptr->data);
		ptr = ptr->next;
	}
	return (cur_max);
}
