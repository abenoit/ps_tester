/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstnew.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abenoit <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/06 17:26:16 by abenoit           #+#    #+#             */
/*   Updated: 2021/03/05 12:08:10 by abenoit          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lib_lst.h"
#include <stdlib.h>

t_stack	*ft_lstnew(int *data)
{
	t_stack *new;

	if (!(new = malloc(sizeof(t_stack))))
		return (NULL);
	new->data = data;
	new->next = NULL;
	return (new);
}
