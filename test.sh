
launch_push () {
ARG=$(./create_list $1)

./push_swap $ARG
}

last_test () {
ARG=$(tail -n1 logs/prev.txt | tr -d 'ARG"=')
#ARG="9507 -7452 -9968 1279 3689 7669 937 -2741 8742 3947 8657 -5254 6432 -1836 6661 9792 554 1216 5582 212 8195 1162 3432 8079 2201 6645 4135 3818 1580 5717 "

./push_swap $ARG | ./checker $ARG
./push_swap $ARG | wc -l

}

launch_test () {
ARG=$(./create_list $1)

mkdir -p logs

echo "ARG=\"$ARG\"" >> logs/prev.txt

./push_swap $ARG | ./checker $ARG
./push_swap $ARG | wc -l
}

test_all_size () {
i=1;
if [ -z $1 ]
then
upto=500
else
upto=$1
fi
launch_test $i
while [ ! $? -eq 1 ] && [ $i -lt $upto ]
do
	i=$[$i + 1]
	sleep 0.01
	launch_test $i
done
}


test_one_size () {
i=0;
if [ -z $2 ]
then
	tries=100
else
	tries=$[$2 -1]
fi

launch_test $1

while [ ! $? -eq 1 ] && [ $i -lt $tries ]
do
	i=$[$i + 1]
	sleep 0.01
	launch_test $1
done
}


if [ -z $1 ] || [ $1 == "-" ]
then
	test_all_size $2
elif [[ $1 == "last" ]]
then
	last_test
elif [[ $2 == "p" ]]
then
	launch_push $1
else
	test_one_size $1 $2
fi
